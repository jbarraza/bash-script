#!/bin/bash

#___________________________________________________________________________________________________
#                       This is a bash script for matching folders of samples 
#___________________________________________________________________________________________ryse-amp

# Parse outputs & declarations

echo "Script name is: $0"
echo " "
echo "you are usign the script for link the information that comes from rucio and the local folders (CxAOD samples)"
echo " "

echo "This is the retrieve information from rucio "
echo " "
rucio list-files group.phys-hdbs.mc16_13TeV.700320.Sh_2211_Zee_maxHTpTV2_BFilter.HIGG4D2.16IsoCRBulb1_CxAOD.root | cut -b 19-61 | grep -v "SCOPE" | grep -i ".root" > mc16_13TeV.700320.Sh_2211_Zee_maxHTpTV2_BFilter.HIGG4D2.16IsoCRBulb1_CxAOD_Nosort.txt

while read line; do

  echo $line

done < mc16_13TeV.700320.Sh_2211_Zee_maxHTpTV2_BFilter.HIGG4D2.16IsoCRBulb1_CxAOD_Nosort.txt

sort mc16_13TeV.700320.Sh_2211_Zee_maxHTpTV2_BFilter.HIGG4D2.16IsoCRBulb1_CxAOD_Nosort.txt > mc16_13TeV.700320.Sh_2211_Zee_maxHTpTV2_BFilter.HIGG4D2.16IsoCRBulb1_CxAOD_1.txt

echo "This is the content of the folder: "
echo " "

ls /eos/atlas/atlascerngroupdisk/phys-hdbs/diHiggs/bbtautau/lephad/Bulbasaur/group.phys-hdbs.mc16_13TeV.700320.Sh_2211_Zee_maxHTpTV2_BFilter.HIGG4D2.16IsoCRBulb1_CxAOD.root > mc16_13TeV.700320.Sh_2211_Zee_maxHTpTV2_BFilter.HIGG4D2.16IsoCRBulb1_CxAOD_2.txt

while read line2; do

  echo $line2

done < mc16_13TeV.700320.Sh_2211_Zee_maxHTpTV2_BFilter.HIGG4D2.16IsoCRBulb1_CxAOD_2.txt

diff -s mc16_13TeV.700320.Sh_2211_Zee_maxHTpTV2_BFilter.HIGG4D2.16IsoCRBulb1_CxAOD_1.txt mc16_13TeV.700320.Sh_2211_Zee_maxHTpTV2_BFilter.HIGG4D2.16IsoCRBulb1_CxAOD_2.txt
